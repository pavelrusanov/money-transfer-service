# money-transfer-service
Test assignment for Revolut

## Asumptions:

1. All accounts are in the same currency, so that parameter is ommited.
2. No user data management implemented. It's possible to create accounts for any
user Id.
3. No account lifecycle is implemented. Only account creation and balance manipulation
methods are provided. There is no _close account_ operation.
4. User Ids and Account Ids are Java long values;

## Build and run
To build application use: `mvn clean package` . Than you can run it using:
```console
 java -jar target/money-transfer-service-1.0.0.jar
```
Server will be started and using default port __8080__.
You can use any port by passing argument __-p__
Logs are printed to the console and to ./logs/money-transfer.log file

## Test
To test App functionality use curl tool

### Create new user account
Create new account for user 1:
```console
curl --request POST --url http://localhost:8080/user/1/account
```
Service will return new account id

### Read account data
Read user 1 account 0:
```console
curl --request GET --url http://localhost:8080/user/1/account/0
```

### Deposit account
Deposit money from user 1 account 0:
```console
curl --request PUT \
--url http://localhost:8080/user/1/account/0/operation \
--header 'content-type: application/json' \
--data '{"amount" : "20.40",	"operationType" : "DEPOSIT"}'
```
### Withdraw account
Withdraw money from user 1 account 0:
```console
curl --request PUT \
--url http://localhost:8080/user/1/account/0/operation \
--header 'content-type: application/json' \
--data '{"amount" : "0.40","operationType" : "WITHDRAW"}'
```
### Transfer money between accounts
Transfer between user 1 account 0 and user 2 account 1):
```console
curl --request PUT \
--url http://localhost:8080/user/1/account/0/operation \
--header 'content-type: application/json' \
--data '{"amount" : "10.21","destAccountId" : 1,"operationType" : "TRANSFER"}'
```




