package com.rusanov.moneytransfer.resource;

import com.rusanov.exception.AccountException;
import com.rusanov.model.OperationData;
import com.rusanov.model.SimpleAccount;
import com.rusanov.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class AccountsRestAPI {
    private static final Logger logger = LoggerFactory.getLogger(AccountsRestAPI.class);

    private AccountService accountService;

    public AccountsRestAPI(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * Return all avaliable accounts for given user ID
     */
    @GET
    @Path("{userId}/account/{accountId}")
    public Response getUserAccount(@PathParam("userId") long userId, @PathParam("accountId") long accountId) {
        logger.info("Get user account {} for user with id: {}",accountId, userId);

        SimpleAccount account = accountService.getUserAccount(userId, accountId);
        if (account != null) {
            return Response.ok().entity(account).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity("User/account not found")
                    .build();
        }

    }

    /**
     * Create new user account
     */
    @POST
    @Path("{userId}/account")
    public Response createUserAccount(@PathParam("userId") long userId) {
        logger.info("Create account for user with id: {}", userId);


        try {
            long createdAccountId = accountService.createAccount(userId);
            logger.info("User {} account {} has been created",userId, createdAccountId);
            return Response.ok()
                    .entity(createdAccountId).build();
        } catch (AccountException e) {
            logger.warn("Exception during account creation",e);
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(e.getUserMessage())
                    .build();
        }


    }


    /**
     * Account manipulation
     *
     */
    @PUT
    @Path("{userId}/account/{accountId}/operation")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response doOperationOnAccount(@PathParam("userId") long userId,
                                         @PathParam("accountId") long accountId,
                                         OperationData operationData) {
        Response response = Response.status(Response.Status.NO_CONTENT).build();

        logger.info("Operation {} for user {} account {}", operationData.getOperationType(), accountId, userId);
        try {
            switch (operationData.getOperationType()) {
                case DEPOSIT:
                    accountService.depositMoney(userId, accountId,operationData.getAmount());
                    break;
                case TRANSFER:
                    accountService.transferMoney(userId, accountId, operationData.getDestAccountId(), operationData.getAmount());
                    break;
                case WITHDRAW:
                    accountService.withdrawMoney(userId, accountId,operationData.getAmount());
                    break;
                default:
                    response = Response
                            .status(Response.Status.BAD_REQUEST)
                            .entity(String.format("Operation %s not supported", operationData.getOperationType())).build();

            }
            return response;

        } catch (AccountException ae) {
            logger.warn("Exception during account manipulation",ae);
            response = Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ae.getUserMessage())
                    .build();
        }

        return response;

    }

}
