package com.rusanov;

import com.rusanov.moneytransfer.resource.AccountsRestAPI;

import com.rusanov.service.AccountService;
import com.rusanov.service.AccountServiceImpl;
import com.rusanov.storage.AccountDAO;
import com.rusanov.storage.AccountDAOImpl;
import com.rusanov.storage.AppDataSource;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpContainer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ContainerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.logging.LogManager;

/**
 * money-transfer-service main class
 *
 * @author pavelrusanov
 */
public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    public static final String BASE_URI_TEMPLATE = "http://localhost:%d/";
    public static final int DEFAULT_PORT = 8080;

    private static Map<Class,Object> applicationCtx;

    static {
        LogManager.getLogManager().reset();
        SLF4JBridgeHandler.install();
    }


    private static void initAppContext(){
        applicationCtx = new HashMap<>();
        applicationCtx.put(AppDataSource.class, new AppDataSource());
        applicationCtx.put(AccountDAO.class, new AccountDAOImpl((AppDataSource) applicationCtx.get(AppDataSource.class)));
        applicationCtx.put(AccountService.class, new AccountServiceImpl((AccountDAO) applicationCtx.get(AccountDAO.class)));
    }

    private static int readPortArgument(String[] args) throws IllegalArgumentException{
        final String portArgName = "-p";
        for(int i = 0; i< args.length; i++){
            if(args[i].equals(portArgName)){
                i++;
                try {
                    int portNum = Integer.valueOf(args[i]);
                    logger.info("Port argument provided {}", portNum);
                    return portNum;
                }catch (NumberFormatException nfe){
                    throw new IllegalArgumentException(String.format("Wrong porn number %s! Correct usage -p port_num",args[i]));
                }
            }
        }
        logger.info("No port argument provided (-p), using default port {}",DEFAULT_PORT);
        return DEFAULT_PORT;
    }

    public static void main(String[] args) throws IOException {
        //TODO read port param
        logger.info("Money-transfer-server application");
        int port = readPortArgument(args);
        String uri = String.format(BASE_URI_TEMPLATE,port);
        initAppContext();

        Set<Object> restServecies = new HashSet<>();

//        restServecies.add(new AccountsRestAPI(new AccountServiceImpl(new AccountDAOImpl(new AppDataSource()))));
        restServecies.add(new AccountsRestAPI((AccountService) applicationCtx.get(AccountService.class)));

        final HttpServer server = startServer(restServecies,uri);
        logger.info("App is running. {}", uri);
        System.in.read();
        server.shutdownNow();
    }

    /**
     * Starts Grizzly HTTP server .
     * @param restServices - rest resource objects
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer(Collection<Object> restServices, String uri) {
        final ResourceConfig rc = new ResourceConfig();

        rc.packages("org.glassfish.jersey.examples.jackson")
                .register(JacksonFeature.class);

        restServices.forEach(restService -> rc.registerInstances(restService));
        GrizzlyHttpContainer resourceConfigContainer = ContainerFactory
                .createContainer(GrizzlyHttpContainer.class, rc);
        HttpServer server = GrizzlyHttpServerFactory.createHttpServer(URI.create(uri));
        server.getServerConfiguration().addHttpHandler(resourceConfigContainer, "/");

        return server;
    }


}
