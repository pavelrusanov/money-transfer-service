package com.rusanov.storage;

import com.rusanov.model.SimpleAccount;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * In-memory data source for App
 *
 * @author pavelrusanov
 */
public class AppDataSource {
    private final ConcurrentMap<Long,SimpleAccount> accountsById;

    private final AtomicLong ACCOUNT_SEQUENCE;

    public AppDataSource() {
        accountsById = new ConcurrentHashMap<>();

        ACCOUNT_SEQUENCE = new AtomicLong(0);
    }


    public ConcurrentMap<Long, SimpleAccount> getAccountsById() {
        return accountsById;
    }

    public long getNextAccountId(){
        return ACCOUNT_SEQUENCE.getAndIncrement();
    }

    public void resetAccountSequense(){
        this.ACCOUNT_SEQUENCE.set(0);
    }
}
