package com.rusanov.storage;

import com.rusanov.model.SimpleAccount;

/**
 * @author pavelrusanov
 */
public class AccountDAOImpl implements AccountDAO {

    private AppDataSource appDataSource;

    public AccountDAOImpl(AppDataSource appDataSource) {
        this.appDataSource = appDataSource;
    }

    @Override
    public long createUserAccount(long userId) {
        SimpleAccount newAccount = new SimpleAccount(appDataSource.getNextAccountId(),userId);
        appDataSource.getAccountsById().put(newAccount.getId(),newAccount);

        return newAccount.getId();
    }

    @Override
    public SimpleAccount getUserAccount(long userId, long accountId) {
        SimpleAccount accountWithId =  appDataSource.getAccountsById().get(accountId);
        if(accountWithId != null && accountWithId.getUserId() == userId){
            return accountWithId;
        }else {
            return null;
        }
    }

    @Override
    public SimpleAccount getAccount(long accountId) {
        return appDataSource.getAccountsById().get(accountId);
    }
}
