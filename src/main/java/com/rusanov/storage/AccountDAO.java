package com.rusanov.storage;

import com.rusanov.model.SimpleAccount;


/**
 * @author pavelrusanov
 */
public interface AccountDAO {
    /**
     * Create new user account with zero balance
     * @param userId
     * @return created account id
     */
    long createUserAccount(long userId);


    /**
     * Get account with given id, which belong to given user id
     * @param userId
     * @param accountId
     * @return account object
     */
    SimpleAccount getUserAccount(long userId, long accountId);

    /**
     *
     * Get account with given id
     * @param accountId
     * @return
     */
    SimpleAccount getAccount(long accountId);


}
