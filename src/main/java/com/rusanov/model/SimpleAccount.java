package com.rusanov.model;

import com.rusanov.exception.AccountException;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Account object.
 * Balance manipulations (deposit/withdraw) are thread safe
 *
 * @author pavelrusanov
 */
public class SimpleAccount {
    private final long id;
    private final long userId;

    private final AtomicReference<BigDecimal> balance;

    public SimpleAccount(long id, long userId) {
        this.id = id;
        this.userId = userId;
        balance = new AtomicReference<>(BigDecimal.ZERO);
    }


    public BigDecimal getBalance() {
        return balance.get();
    }

    public long getUserId() {
        return userId;
    }

    public long getId() {
        return id;
    }


    /**
     * Decrease account balance for given amount of money.
     * Operation is thread safe
     * @param amount - money to withdraw
     * @throws AccountException if there is not enough money
     */
    public void withdrawBalance(BigDecimal amount) throws AccountException {
        boolean success = false;
        BigDecimal currentBalance;
        BigDecimal afterDecrease;

        while (!success){
            currentBalance = balance.get();
            afterDecrease = currentBalance.subtract(amount);
            if(afterDecrease.compareTo(BigDecimal.ZERO)== -1){
                throw new AccountException("Account balance could not be less than 0!",
                        String.format("Not enough money on account # %s ", id));
            }

            success = balance.compareAndSet(currentBalance,afterDecrease);

        }

    }

    /**
     * Deposit account balance with given amount of money
     * Operation is thread safe
     * @param amount -  money to withdraw
     */
    public void increaseBalance(BigDecimal amount){
        boolean success = false;
        BigDecimal currentBalance;
        BigDecimal afterDecrease;

        while (!success){
            currentBalance = balance.get();
            afterDecrease = currentBalance.add(amount);
            success = balance.compareAndSet(currentBalance,afterDecrease);
        }
    }


}
