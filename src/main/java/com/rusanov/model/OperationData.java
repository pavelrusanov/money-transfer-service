package com.rusanov.model;

import java.math.BigDecimal;

/**
 * Account manipulation details
 *
 * @author pavelrusanov
 */
public class OperationData {
    private BigDecimal amount;
    private long destAccountId;
    private AccountOperationType operationType;


    public BigDecimal getAmount() {
        return amount;
    }

    public long getDestAccountId() {
        return destAccountId;
    }

    public AccountOperationType getOperationType() {
        return operationType;
    }


}
