package com.rusanov.model;

/**
 * Account operations types
 *
 * @author pavelrusanov
 */
public enum  AccountOperationType {
    DEPOSIT,TRANSFER,WITHDRAW
}
