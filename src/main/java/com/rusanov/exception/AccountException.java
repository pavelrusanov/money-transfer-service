package com.rusanov.exception;

/**
 * Exception during account manipulations
 *
 * @author pavelrusanov
 */
public class AccountException extends Exception {
    /**
     * Human readable message
     */
    private String userMessage;

    public AccountException(String message, String userMessage) {
        super(message);
        this.userMessage = userMessage;
    }

    public AccountException(String message) {
        super(message);
        this.userMessage = message;
    }

    public String getUserMessage() {
        return userMessage;
    }
}
