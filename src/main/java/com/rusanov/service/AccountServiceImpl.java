package com.rusanov.service;

import com.rusanov.exception.AccountException;
import com.rusanov.model.SimpleAccount;
import com.rusanov.storage.AccountDAO;

import java.math.BigDecimal;

/**
 * @author pavelrusanov
 */
public class AccountServiceImpl implements AccountService{
    private AccountDAO accountDAO;

    public AccountServiceImpl(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    @Override
    public void transferMoney(long sourceAccountOwner, long sourceAccountId, long destinationAccountId, BigDecimal amount) throws AccountException {
        if(sourceAccountId == destinationAccountId){
            throw new AccountException("Can't transfer money between same accounts!");
        }

        checkAmountToOperate(amount);

        SimpleAccount sourceAccount = getAndCheckAccount(sourceAccountOwner, sourceAccountId);
        SimpleAccount destAccount = getAndCheckAccount(destinationAccountId);


        sourceAccount.withdrawBalance(amount);
        destAccount.increaseBalance(amount);

    }

    @Override
    public void depositMoney(long accountOwner, long accountId, BigDecimal amount) throws AccountException {
        checkAmountToOperate(amount);
        SimpleAccount accountToDeposit = getAndCheckAccount(accountOwner, accountId);

        accountToDeposit.increaseBalance(amount);

    }

    @Override
    public void withdrawMoney(long accountOwner, long accountId, BigDecimal amount) throws AccountException {
        checkAmountToOperate(amount);
        SimpleAccount accountToWithdraw = getAndCheckAccount(accountOwner, accountId);

        accountToWithdraw.withdrawBalance(amount);
    }

    @Override
    public long createAccount(long userId) {
        return accountDAO.createUserAccount(userId);
    }

    @Override
    public SimpleAccount getUserAccount(long userId, long accountId) {
        return accountDAO.getUserAccount(userId,accountId);
    }


    private SimpleAccount getAndCheckAccount(long accountId) throws AccountException{
        SimpleAccount account = accountDAO.getAccount(accountId);
        if(account == null){
            throw new AccountException(String.format("Account %d not found!",accountId));
        }else {
            return account;
        }
    }

    private SimpleAccount getAndCheckAccount(long userId, long accountId) throws AccountException {
        SimpleAccount account = getAndCheckAccount(accountId);
        if(account.getUserId() != userId){
            throw new AccountException(String.format("Account %d does not belongs to user %d!", accountId, userId));
        }else {
            return account;
        }
    }

    private void checkAmountToOperate(BigDecimal amount) throws AccountException {
        if(amount.compareTo(BigDecimal.ZERO) <= 0 ){
            throw new AccountException(String.format("Can't operate with negative or ZERO numbers %s !", amount));
        }
    }


}
