package com.rusanov.service;


import com.rusanov.exception.AccountException;
import com.rusanov.model.SimpleAccount;

import java.math.BigDecimal;

/**
 * @author pavelrusanov
 */
public interface AccountService {

    /**
     * Transfer money between accounts (thread safe)
     * @param sourceAccountOwner - user which owns account
     * @param sourceAccountId - source of money to transfer
     * @param destinationAccountId - account to transfer
     * @param amount - money to transfer
     * @throws AccountException if there is problem with operation (no source user/account found,
     * not enough money, incorrect amount, etc..)
     */
    void transferMoney(long sourceAccountOwner, long sourceAccountId, long destinationAccountId, BigDecimal amount) throws AccountException;

    /**
     * Deposit money
     * @param accountOwner - user which owns account
     * @param accountId - id of account
     * @param amount - money to deposit
     * @throws AccountException if there is problem with operation (no user/account found,
     * not enough money, incorrect amount, etc..)
     */
    void depositMoney(long accountOwner, long accountId, BigDecimal amount) throws AccountException;

    /**
     * Withdraw money from given user account
     * @param accountOwner - user which owns account
     * @param accountId - id of account
     * @param amount - money to withdraw
     * @throws AccountException if there is problem with operation (no user/account found,
     * not enough money, incorrect amount, etc..)
     */
    void withdrawMoney(long accountOwner, long accountId, BigDecimal amount) throws AccountException;

    /**
     * Creates new account for given user Id
     * @return id of new account
     * @throws AccountException if there is no user, or there is problems while creating account
     */
    long createAccount(long userId) throws AccountException;

    /**
     * Get account info
     */
    SimpleAccount getUserAccount(long userId, long accountId);

}
