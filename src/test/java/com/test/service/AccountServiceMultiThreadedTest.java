package com.test.service;

import com.rusanov.exception.AccountException;
import com.rusanov.model.SimpleAccount;
import com.rusanov.service.AccountService;
import com.rusanov.service.AccountServiceImpl;
import com.rusanov.storage.AccountDAO;
import com.rusanov.storage.AccountDAOImpl;
import com.rusanov.storage.AppDataSource;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;


public class AccountServiceMultiThreadedTest {
    //TODO Rusanov: disable parallel test execution for that class in maven
    private static final Logger logger = LoggerFactory.getLogger(AccountServiceMultiThreadedTest.class);
    private final AppDataSource appDataSource;
    private final AccountDAO accountDAO;

    private final AccountService accountService;

    Map<Long, List<Long>> testUsersAccouts;

    Map<Long, Long> accountWithUser;

    public AccountServiceMultiThreadedTest() {
        appDataSource = new AppDataSource();
        accountDAO = new AccountDAOImpl(appDataSource);
        accountService = new AccountServiceImpl(accountDAO);

        testUsersAccouts = new HashMap<>();
        accountWithUser = new HashMap<>();
    }

    @Before
    public void clearAndSetupTestData() {
        clearDataSource();

        long userId = 1;
        accountWithUser.put(accountDAO.createUserAccount(userId), userId);
        accountWithUser.put(accountDAO.createUserAccount(userId), userId);

        userId = 2;
        accountWithUser.put(accountDAO.createUserAccount(userId), userId);
        accountWithUser.put(accountDAO.createUserAccount(userId), userId);

        userId = 3;
        accountWithUser.put(accountDAO.createUserAccount(userId), userId);
        accountWithUser.put(accountDAO.createUserAccount(userId), userId);

    }

    private void clearDataSource() {
        appDataSource.getAccountsById().clear();
        appDataSource.resetAccountSequense();
    }


    private Map<Long, BigDecimal> getAccountsBalances(Map<Long, Long> accountWithUser) {
        Map<Long, BigDecimal> accountBalances = new HashMap<>(accountWithUser.size());
        for (Entry<Long, Long> accountAndUser : accountWithUser.entrySet()) {
            BigDecimal currentBalance = accountService.getUserAccount(
                    accountAndUser.getValue(),
                    accountAndUser.getKey()
            ).getBalance();

            accountBalances.put(accountAndUser.getKey(), currentBalance);
        }
        return accountBalances;
    }


    public void depositAccounts(Map<Long, Long> accountWithUser, int amount) throws AccountException {
        final BigDecimal BALANCE_100000 = new BigDecimal(amount);
        for (Entry<Long, Long> accountAndUser : accountWithUser.entrySet()) {
            accountService.depositMoney(accountAndUser.getValue(), accountAndUser.getKey(), BALANCE_100000);
        }
    }

    /**
     * Calcs sum of balances of accounts before and after transfers, sums must be equal
     *
     * @param balancesBefore - balance before transfer
     * @param balancesAfter  - balance after transfer
     */
    private void checkTrasfersSumCorrect(Map<Long, BigDecimal> balancesBefore, Map<Long, BigDecimal> balancesAfter) {
        //Sum of all balances BEFORE transfers must be equal sum of all balances AFTER
        final BigDecimal sumBefore = balancesBefore.values().stream().reduce(BigDecimal.ZERO, BigDecimal::add);
        final BigDecimal sumAfter = balancesAfter.values().stream().reduce(BigDecimal.ZERO, BigDecimal::add);

        assertEquals(sumBefore, sumAfter);
    }

    @Test
    public void testConcurrentTransfer() throws AccountException, InterruptedException {
        //Create transfer log (plan for transfer money between accounts;
        final int numOfTransfers = 1000;
        final int maxMoneyToTransfer = 100;

        List<TransferData> transferLog = new ArrayList<>(numOfTransfers);
        for (int i = 0; i < numOfTransfers; i++) {
            transferLog.add(TransferData.generateRandomData(accountWithUser.size(), maxMoneyToTransfer));
        }

        //Deposit accounts to have enough money (numOfTransfers * maxMoneyToTransfer = 100 000)
        depositAccounts(accountWithUser, numOfTransfers * maxMoneyToTransfer);

        Map<Long, BigDecimal> initalBalances = getAccountsBalances(accountWithUser);

        //Make all transfers in single thread
        for (TransferData transferData : transferLog) {
            accountService.transferMoney(
                    accountWithUser.get(transferData.from),
                    transferData.from,
                    transferData.to,
                    transferData.amount
            );
        }

        //Get account balances & clear
        Map<Long, BigDecimal> accBalancesAfterTranserSingleThread = getAccountsBalances(accountWithUser);
        checkTrasfersSumCorrect(initalBalances, accBalancesAfterTranserSingleThread);

        //Clear data and deposit again
        clearAndSetupTestData();
        depositAccounts(accountWithUser, numOfTransfers * maxMoneyToTransfer);

        ExecutorService es = Executors.newFixedThreadPool(30);
        for (TransferData transferData : transferLog) {
            es.submit(new TransferMoneyTask(transferData));
        }

        es.shutdown();
        boolean allTransfersDone = es.awaitTermination(20, TimeUnit.SECONDS);

        if (allTransfersDone) {
            Map<Long, BigDecimal> accBalancesAfterTranserMultiThread = getAccountsBalances(accountWithUser);
            checkTrasfersSumCorrect(initalBalances, accBalancesAfterTranserMultiThread);

            //Check if results are the same
            for (Entry<Long, BigDecimal> accountBalance : accBalancesAfterTranserMultiThread.entrySet()) {
                assertEquals(
                        String.format(
                                "Account %d should has same balance in single thread transfers and in multi thread",
                                accountBalance.getKey()
                        ),
                        accBalancesAfterTranserSingleThread.get(accountBalance.getKey()),
                        accountBalance.getValue()

                );
            }

        } else {
            throw new RuntimeException("Normaly thread pool should finish all transfers in 20 seconds ");

        }

    }

    @Test
    public void testDeposit() throws InterruptedException {

        final long accountIdToTest = 0;
        final long userIdToTest = accountWithUser.get(accountIdToTest);

        final int numOfDeposits = 1000;
        final int maxMoneyPerDeposit = 1000;
        Random r = new Random();
        List<Integer> depositValues = new ArrayList<>(numOfDeposits);

        for (int i = 0; i < numOfDeposits; i++) {
            int value = r.nextInt(maxMoneyPerDeposit);
            depositValues.add(value);
        }

        int sum = depositValues.stream().mapToInt(i -> i).sum();
        BigDecimal expectedDeposit = BigDecimal.valueOf(sum);

        ExecutorService es = Executors.newFixedThreadPool(30);
        for (Integer moneyToDeposit : depositValues) {
            es.submit(() -> {
                try {
                    accountService.depositMoney(userIdToTest, accountIdToTest, BigDecimal.valueOf(moneyToDeposit));
                } catch (AccountException e) {
                    throw new RuntimeException(e);
                }
            });
        }

        es.shutdown();

        boolean allDepositsDone = es.awaitTermination(20, TimeUnit.SECONDS);
        if (allDepositsDone) {
            SimpleAccount testAcc = accountService.getUserAccount(userIdToTest, accountIdToTest);
            assertEquals(expectedDeposit, testAcc.getBalance());
        } else {
            throw new RuntimeException("Normaly thread pool should finish all transfers in 20 seconds ");
        }

    }

    @Ignore
    @Test
    public void testWithdraw() throws InterruptedException {
        //TODO implement me;
        //Same as deposit, bit prior test you must deposit account with enough money
    }

    /**
     * Transfer log data.
     */
    private static class TransferData {
        private long from;
        private long to;
        private BigDecimal amount;

        private static Random r = new Random();

        private TransferData(long from, long to, BigDecimal amount) {
            this.from = from;
            this.to = to;
            this.amount = amount;
        }

        private static TransferData generateRandomData(int idsBount, int amountBound) {
            int from, to, amount;
            do {
                //Transfer must be performed between different accounts
                from = r.nextInt(idsBount);
                to = r.nextInt(idsBount);
            } while (from == to);


            do {
                amount = r.nextInt(amountBound);
            } while (amount == 0);

            return new TransferData(from, to, BigDecimal.valueOf(amount));
        }
    }

    private class TransferMoneyTask implements Runnable {
        private TransferData transferData;

        public TransferMoneyTask(TransferData transferData) {
            this.transferData = transferData;
        }

        @Override
        public void run() {
            try {
                accountService.transferMoney(
                        accountWithUser.get(transferData.from),
                        transferData.from,
                        transferData.to,
                        transferData.amount
                );
            } catch (AccountException e) {
                throw new RuntimeException(e);
            }
        }
    }


}

